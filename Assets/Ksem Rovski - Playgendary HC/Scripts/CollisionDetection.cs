﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    [SerializeField]
    private GameObject MeshVisualLink;
    [SerializeField]
    private GameObject OnDeathFX;
    [SerializeField]
    private GameObject GameManagerLink;

    void Start()
    {
        //Нашли менеджер с статичными/глобальными скриптами и прочими переменными
        GameManagerLink = GameObject.FindWithTag("GameManager");
    }

    void OnCollisionEnter(Collision col)
    {
        //Проверка по тегу стокнулась ли клубника с коллайдером оружия игрока
        if (col.gameObject.tag == "HitCollider")
        {
            //Выключили меш модели
            //Включили систему частиц
            MeshVisualLink.SetActive(false);
            OnDeathFX.SetActive(true);

            //Выключение коллайдера, чтобы лишний раз не триггерить коллизию
            this.gameObject.GetComponent<BoxCollider>().enabled = false;

            //Вызвали анимацию тряски камеры
            //Запустили задержку перед удалением объекта
            GameManagerLink.GetComponent<GameManager>().ShakeCamera();
            StartCoroutine(Wait(1.5f));

        }

        //Если игрок не попал по клубнике и она залетела в ворота, то отнимаем жизнь
        if (col.gameObject.tag == "LoseCollider")
        {
            GameManagerLink.GetComponent<GameManager>().LosePoint();
            GameManagerLink.GetComponent<GameManager>().SpawnNextEnemy();
            Destroy(this.gameObject);
        }
    }

    private IEnumerator Wait(float waitTime)
    {
        //Немного ждём, удаляем этот объект (клубнику), а затем вызываем спаун следующего объекта
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
        GameManagerLink.GetComponent<GameManager>().SpawnNextEnemy();
    }
}
