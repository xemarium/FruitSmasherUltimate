﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSkybox : MonoBehaviour
{
    [SerializeField]
    private float adjustmentAmount = 1f;

    void Update()
    {
        //Поворот скайбокса по обращению к _RotationX переменной внутри шейдера
        //
        RenderSettings.skybox.SetFloat("_RotationX", Time.time * adjustmentAmount);
    }
}
