﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Transform SpawnPoint;
    [SerializeField]
    private GameObject NextEnemy;
    [SerializeField]
    private GameObject CameraLink;
    [SerializeField]
    private Text ScoreTextLink;
    [SerializeField]
    private Text LivesTextLink;
    [SerializeField]
    private GameObject LoseScreenPanel;
    
    private int Score = 0;
    private int LivesLeft = 4;

    //Публичная функция тряски камеры через анимацию
    public void ShakeCamera()
    {
        AddScore();
        CameraLink.GetComponent<Animation>().Play();
    }

    private void AddScore()
    {
        //Тут начисляется очко за попадание по клубнике + обновление UI текста с количеством очков
        Score++;
        ScoreTextLink.text = "Score : " + Score.ToString();
    }

    public void LosePoint()
    {
        //Тут отнимается одна жизнь за пропуск клубники
        LivesLeft-=1;
        Score-=1;
        LivesTextLink.text = "Lives : " + LivesLeft.ToString();

        //Если жизни кончились - игрок проиграл, показываем панель с кнопкой рестарта
        if (LivesLeft == 0)
        {
            LoseScreenPanel.SetActive(true);
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("FruitSmasherUltimate");
    }

    //Публичная функция спауна следующего врага (клубники) на точке спауна 
    //Благодаря рандому клубника появляется в пределах 1 юнита по Z координате (для разнообразия)
    public void SpawnNextEnemy()
    {
        Instantiate(NextEnemy, SpawnPoint.position + new Vector3(0,0,Random.Range(-0.5f,0.5f)), Quaternion.identity);
    }
}
