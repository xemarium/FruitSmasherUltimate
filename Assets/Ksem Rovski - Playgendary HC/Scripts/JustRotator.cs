﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustRotator : MonoBehaviour
{
    [SerializeField]
    private float RotateSpeed = 45;

    private Transform RotationEntity;

    void Start()
    {
        RotationEntity = this.gameObject.GetComponent<Transform>();
    }

    void Update()
    {
        transform.RotateAround(RotationEntity.position, Vector3.up, RotateSpeed * Time.deltaTime);
    }
}
