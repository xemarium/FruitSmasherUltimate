﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantForce : MonoBehaviour
{
    [SerializeField]
    private float MoveSpeed = 1;

    void Start()
    {
        //Рандомизируем скорость движения клубники ради разнообразия геймплея
        MoveSpeed += Random.Range(0, 5);
    }

    void Update()
    {
        //Двигаем клубнику в сторону игрока
        transform.Translate(Vector3.left * Time.deltaTime * MoveSpeed);
    }
}
